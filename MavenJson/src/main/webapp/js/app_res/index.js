function updateOk(){
	$.ajax({ 
		type: "POST", 
		url: "/MavenJson/updateUser", 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(updateJsonDataOk()), 
		dataType: "json",
		success:function(data){
			window.location.reload();
		}
	});
	
}
function updateJsonDataOk(){
	
	var jsondata = {
			"userId":$("#idCard").val(),
			"userName":$("#username").val(),
			"userSalary":$("#Salary").val()
	};
	return jsondata;
}
function deleteUser(){//删除数据
	
	$.ajax({ 
		type: "POST", 
		url: "/MavenJson/deleteUser", 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(deleteJsonData()), 
		dataType: "json",
		success:function(data){
			window.location.reload();
		}
	});
}
function deleteJsonData(){
	var obj=document.getElementsByName('userCheck'); //选择所有name="'test'"的对象，返回数组 
	//取到对象数组后，我们来循环检测它是不是被选中  
	for(var i=0; i<obj.length; i++){ 
		if(obj[i].checked) {
			/**
			*通过post方式，向后台提交参数
			json数组的格式，必须和接受对象的属性是一样的
			如userId = User.userId
			**/
			var jsondata = {
					"userId":obj[i].value,
			};
		}
	}
	
	return jsondata;
}


function updateUser(){//修改数据
	
	$.ajax({ 
		type: "POST", 
		url: "/MavenJson/getUserById", 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(updateJsonData()), 
		dataType: "json",
		success:function(data){
			$("#idCard").val(data.userId).css("color","#888"); ;
			$("input[name=idCard]").attr("readonly","readonly");
			$("#username").val(data.userName) ;
			$("#Salary").val(data.userSalary) ;
			 var addSpan=document.getElementById("addSpan");  
			 addSpan.innerHTML = "<input type='button' style='color: blue;' value='修改确认' onclick='updateOk()'>"
		}
	});
}
function updateJsonData(){
	
	if($("input[type='checkbox']:checked").length >1){
		alert("有且只有选择一行数据");
		return false;
	}
	var obj=document.getElementsByName('userCheck'); //选择所有name="'test'"的对象，返回数组 
	var userId = "";
	for (var i = 0; i < obj.length; i++) {
		if(obj[i].checked){
			userId = obj[i].value;
		}
	}
	
	var jsondata = {
			"userId":userId
	};
	return jsondata;
}




	 


function register(){//注册数据
	$.ajax({ 
		type: "POST", 
		url: "/MavenJson/register", 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(GetJsonData()), 
		dataType: "json",
		success:function(data){
			
			 if("false" == data){
		        	alert("数据格式不对，无法注册，请重新检查");
		        }else{
		        	window.location.reload();
		       }
			
		}
	});
}
function GetJsonData(){
	/**
	*通过post方式，向后台提交参数
	json数组的格式，必须和接受对象的属性是一样的
	如userId = User.userId
	**/
	var jsondata = {
			"userId":$("#idCard").val(),
			"userName":$("#username").val(),
			"userSalary":$("#Salary").val()
	};
	return jsondata;
}