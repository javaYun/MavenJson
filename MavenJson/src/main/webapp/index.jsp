<%@ page language="java" pageEncoding="UTF-8"%>
<%--引入JSTL核心标签库 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
    <%  
    String path = request.getContextPath();  
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
      
%>
<script type="text/javascript" src="<%=basePath%>js/jquery/jquery.js"></script>
<script type="text/javascript" src="<%=basePath%>js/jquery/jquery.easyui.js"></script>  
<script type="text/javascript" src="<%=basePath%>js/jquery/easyui-lang-zh_CN.js"></script>  

<script type="text/javascript" src="<%=basePath%>js/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/app_res/index.js"></script>

<head>
<script>
$(document).ready(function(){
	 var tbody=document.getElementById("table-result");  
	 $.ajax({ 
			type: "POST", 
			url: "/MavenJson/getAllUserList", 
			contentType: "application/json; charset=utf-8", 
			data: JSON.stringify(GetJsonData()), 
			dataType: "json",
			success: function (data) {  
				  var str="";
				  for (var i = 0; i < data.length; i++) {
					  if(i == 0){
						  str += "<tr><th>选择</th><th>身份证号</th><th>用户名</th><th>薪水</th></tr><tr>" +  
						  "<td><input type='checkBox' name='userCheck' value='"+data[i].userId+"'></td>"+
						  "<td>" + data[i].userId + "</td>" +  
						  "<td>" + data[i].userName + "</td>" +  
						   "<td>" + data[i].userSalary +"￥" + "</td>" +  
						   "</tr>";   
					  }else{
						  
						  str += "<tr>" +  
						  "<td><input type='checkBox' name='userCheck' value='"+data[i].userId+"'></td>"+
						  "<td>" + data[i].userId + "</td>" + 
						  "<td>" + data[i].userName + "</td>" +  
						   "<td>" + data[i].userSalary+"￥" + "</td>" +  
						   "</tr>";  
					  }
					 
				}
				  tbody.innerHTML = str;  
			  }
	
});
})


function displayNewPage(){
	window.open("/MavenJson/newPage", "新窗口");
}
function doUpload(){
	var jsonData = {
			"file":$("#file").val()
	};
	$.ajax({ 
		type: "POST", 
		enctype:"multipart/form-data",
		url: "/MavenJson/doUpload", 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(jsonData), 
		dataType: "json",
		success:function(data){
			
			 alert("上传成功！");
			
		}
	});
}
</script>
<script>




</script>

</head>
<body >

<form id="loginForm" method="post" action="/MavenJson/doUpload"  enctype="multipart/form-data">
<div>
<span style="color: gray;">  身份证号:</span>
<input type="text" size ="20px" id="idCard" name="idCard" /><br>
<span style="color: gray;">  用户姓名:</span>
<input type="text" size ="20px" id="username" name="username" /><br>
<input type="hidden" size ="20px" id="Birthday" name="Birthday" value="2017-01-01"/>
<span style="color: gray;">  个人薪水:</span>
<input type="text" size ="20px" id="Salary" name="Salary" /><br>
<br>
<input type="button" value="注册" onclick="register()">

<input type="button" value="修改" onclick="updateUser()">
<span id="addSpan" ></span>

<input type="button" value="删除" onclick="deleteUser()">
<input type="button" value="新页面展示数据" onclick="displayNewPage()"/>
</div>
<div class="table_user">
<table class="table-result" id="table-result">    
            <tr>  
                <td>身份证号</td>  
                <td>用户姓名</td>  
                <td>薪资水平</td>  
            </tr>  
        </table>  
</div>
<input type="file" id="file" name="file" />
<input type="submit" value="上传文件" />
</form>

</body>
</html>
