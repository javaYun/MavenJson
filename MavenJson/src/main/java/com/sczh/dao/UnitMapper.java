package com.sczh.dao;

import com.sczh.domain.Unit;

public interface UnitMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Unit record);

    int insertSelective(Unit record);

    Unit selectByPrimaryKey(Integer id);

    
    int updateByPrimaryKeySelective(Unit record);

    int updateByPrimaryKey(Unit record);
}