package com.sczh.dao;

import java.util.List;

import com.sczh.domain.Unit;
import com.sczh.domain.User;

public interface UserMapper {
    int deleteByPrimaryKey(String userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    List<User> getAllUser();
    
    List<Unit> getAllUnit();
    
    User getUserByUserName(String username);
}