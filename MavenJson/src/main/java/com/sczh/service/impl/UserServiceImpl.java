package com.sczh.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sczh.dao.UserMapper;
import com.sczh.domain.Unit;
import com.sczh.domain.User;
import com.sczh.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	public void addUser(User user) {
		userMapper.insert(user);

	}

	public User getUserById(String userId) {
		return userMapper.selectByPrimaryKey(userId);
	}

	public List<User> getAllUser() {
		return userMapper.getAllUser();
	}

	public List<Unit> getAllUnit() {
		return userMapper.getAllUnit();
	}

	public User getUserByUserName(String username) {
		return userMapper.getUserByUserName(username);
	}

	public void deleteUser(String userId) {
		userMapper.deleteByPrimaryKey(userId);
		
	}
	public int updateUser(User user) {

		return userMapper.updateByPrimaryKey(user);
	}

}
