package com.sczh.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sczh.domain.User;
import com.sczh.service.UserService;

@Controller
public class UserController  {
	@Autowired
	private UserService userService;
	
	
	@ResponseBody
	@RequestMapping(value = "/getLoginPage",method = RequestMethod.POST,   
			        produces = "text/json;charset=UTF-8")
	public ModelAndView getLoginPage(HttpServletRequest request){
		ModelAndView mav=new ModelAndView("/getLoginPage");;
//		String page = "";
		String username  = request.getParameter("username");
		String password  =request.getParameter("password")==null?"123456":request.getParameter("password");
		User user  = null ;
		if(username !=null && !"".equals(username)&&"123456".equals(password)){
			user = userService.getUserByUserName(username);
			mav.addObject("user", user);
		}
		if(user != null){
			mav.addObject("isEnable", "true");
			mav.addObject("username", username+":欢迎进行系统");
		}else{
			mav.addObject("username", "用户名不存在，无法进入系统");
			mav.addObject("isEnable", "false");
		}
		return mav;
	}
	/**
	 * 添加数据库表到t_user
	 * @param user
	 * @return
	 */
	@ResponseBody //@ResponseBody代表数据以json串的方式传递数据
	@RequestMapping(value = "/register",method=RequestMethod.POST)
	public  String register(@RequestBody User user){//添加到数据库表t_user
		String isRegister = "false";
		if(user != null){
			try {
				userService.addUser(user);
				isRegister = "true";
			} catch (Exception e) {
				isRegister = "false";
			}
		}	
		return isRegister;	
	}
	/**
	 * 获取数据库表t_user所有数据信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllUserList",method = RequestMethod.POST)
	public  List<User> getAllUserList(){
		List<User> list = userService.getAllUser();
		
		return list;
	}
	/**
	 * 根据userId获取User
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserById",method = RequestMethod.POST)
	public User getUserById(@RequestBody User user1){
		User user = userService.getUserById(user1.getUserId());
		return user;
	}
	/**
	 * 添加数据库表到t_user
	 * @param user
	 * @return
	 */
	@ResponseBody //@ResponseBody代表数据以json串的方式传递数据
	@RequestMapping(value = "/deleteUser",method=RequestMethod.POST)
	public  String deleteUser(@RequestBody User user){//添加到数据库表t_user
		String isdelete = "false";
		if(user != null){
			try {
				userService.deleteUser(user.getUserId());
				isdelete = "true";
			} catch (Exception e) {
				isdelete = "false";
			}
		}	
		return isdelete;	
	}
	
	/**
	 * 添加数据库表到t_user
	 * @param user
	 * @return
	 */
	@ResponseBody //@ResponseBody代表数据以json串的方式传递数据
	@RequestMapping(value = "/updateUser",method=RequestMethod.POST)
	public  int updateUser(@RequestBody User user){//添加到数据库表t_user
		return userService.updateUser(user);	
	}
	@ResponseBody //@ResponseBody代表数据以json串的方式传递数据
	@RequestMapping(value = "/newPage")
	public ModelAndView newPage(){
		ModelAndView mav = new ModelAndView("/getNewPage");
		mav.addObject("user", "aaa");
		System.out.println("进入新的页面");
		return mav;
		
	}
	@ResponseBody //@ResponseBody代表数据以json串的方式传递数据
	@RequestMapping(value = "/doUpload")
	public String doUpload(@RequestParam(value="file",required=false) MultipartFile file){
		 if(!file.isEmpty()){  
			         try {     
			        	 String fileName = System.currentTimeMillis()+file.getOriginalFilename();
			        	 System.out.println(fileName+"111");
			                 //这里将上传得到的文件保存至 d:\\temp\\file 目录  
			               FileUtils.copyInputStreamToFile(file.getInputStream(), new File("d:\\temp\\file\\",fileName   
			                         ));  
			            } catch (IOException e) {  
			               e.printStackTrace();  
			           }  
			         }  
			  
			         return "success";  

		
	}
	

}
